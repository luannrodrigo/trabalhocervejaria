var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const db = require('./config/database');

db('mongodb://localhost:27017/fabricacao-cerveja');

//liganco aplicação a rota e associando
const produto = require('./routes/produto');
app.use('/produto', produto);

const lote = require('./routes/lote');
app.use('/lote', lote);

const loteMateriaPrima = require('./routes/loteMateriaPrima');
app.use('/loteMateriaPrima', loteMateriaPrima);

const receita = require('./routes/receita');
app.use('/receita', receita);

const cervejeiro = require('./routes/cervejeiro')
app.use('/cervejeiro', cervejeiro)


module.exports = app;
