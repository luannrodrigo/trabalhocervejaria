const Cervejeiro = require('../models/Cervejeiro')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        Cervejeiro.create(req.body).then(
            // Callback caso a requisição seja certa
            function(){
                res.send(201).end();
            },
            function (erro){
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function(req, res){
        Cervejeiro.find().exec().then(
            // ok
            function(cervejeiros) {
                res.json(cervejeiros).end();
            },
            function (erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function(req, res){
        Cervejeiro.findById(req.params.id).exec().then(
            function (cervejeiros) {
                if (cervejeiros) {
                    res.json(cervejeiros);
                }else {
                    res.status(404).end();
                }

            },
            function(erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.atualizar = function(req, res) {
        Cervejeiro.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function(){
                res.send(204).end();
            },
            function(erro){
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function(req, res) {
        Cervejeiro.findByIdAndRemove(req. params.id).exec().then(
            function() {
                res.send(`Elemento excluido com sucesso ${204}`).end();
            },
            function(erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
