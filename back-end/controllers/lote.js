const Lote = require('../models/Lote')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        Lote.create(req.body).then(
            // Callback caso a requisição seja certa
            function(){
                res.send(201).end();
            },
            function (erro){
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function(req, res){
        Lote.find().exec().then(
            // ok
            function(lotes) {
                res.json(lotes).end();
            },
            function (erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function(req, res){
        Lote.findById(req.params.id).exec().then(
            function (lotes) {
                if (skillss) {
                    res.json(lotes);
                }else {
                    res.status(404).end();
                }

            },
            function(erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.atualizar = function(req, res) {
        Lote.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function(){
                res.send(204).end();
            },
            function(erro){
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function(req, res) {
        Lote.findByIdAndRemove(req. params.id).exec().then(
            function() {
                res.send(204).end();
            },
            function(erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
