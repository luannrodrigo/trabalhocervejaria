const LoteMateriaPrima = require('../models/LoteMateriaPrima')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        LoteMateriaPrima.create(req.body).then(
            // Callback caso a requisição seja certa
            function(){
                res.send(201).end();
            },
            function (erro){
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function(req, res){
        LoteMateriaPrima.find().exec().then(
            // ok
            function(loteMateriaPrimas) {
                res.json(loteMateriaPrimas).end();
            },
            function (erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function(req, res){
        LoteMateriaPrima.findById(req.params.id).exec().then(
            function (loteMateriaPrimas) {
                if (loteMateriaPrimas) {
                    res.json(loteMateriaPrimas);
                }else {
                    res.status(404).end();
                }

            },
            function(erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.atualizar = function(req, res) {
        LoteMateriaPrima.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function(){
                res.send(204).end();
            },
            function(erro){
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function(req, res) {
        LoteMateriaPrima.findByIdAndRemove(req. params.id).exec().then(
            function() {
                res.send(204).end();
            },
            function(erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
