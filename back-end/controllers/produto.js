const Produto = require('../models/Produto')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        Produto.create(req.body).then(
            // Callback caso a requisição seja certa
            function(){
                res.send(201).end();
            },
            function (erro){
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function(req, res){
        Produto.find().populate('users').exec().then(
            // ok
            function(produtos) {
                res.json(produtos).end();
            },
            function (erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function(req, res){
        Produto.findById(req.params.id).exec().then(
            function (produtos) {
                if (produtos) {
                    res.json(produtos);
                }else {
                    res.status(404).end();
                }

            },
            function(erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.atualizar = function(req, res) {
        Produto.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function(){
                res.send(204).end();
            },
            function(erro){
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function(req, res) {
        Produto.findByIdAndRemove(req. params.id).exec().then(
            function() {
                res.send(204).end();
            },
            function(erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
