const Receita = require('../models/Receita')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        Receita.create(req.body).then(
            // Callback caso a requisição seja certa
            function(){
                res.send(201).end();
            },
            function (erro){
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function(req, res){
        Receita.find().exec().then(
            // ok
            function(receitas) {
                res.json(receitas).end();
            },
            function (erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function(req, res){
        Receita.findById(req.params.id).exec().then(
            function (receitas) {
                if (receitas) {
                    res.json(receitas);
                }else {
                    res.status(404).end();
                }

            },
            function(erro){
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.atualizar = function(req, res) {
        Receita.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function(){
                res.send(204).end();
            },
            function(erro){
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function(req, res) {
        Receita.findByIdAndRemove(req. params.id).exec().then(
            function() {
                res.send(204).end();
            },
            function(erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
