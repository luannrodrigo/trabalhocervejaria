const mongoose = require('mongoose');

module.exports = function(){
    const schema = mongoose.Schema({
        nomeLote:{
            type: String,
            required: true
        },
        dataLote:{
            type: Date,
            default: Date.now()
        },
        dataValidade:{
            type: Date,
            default: Date.now()
        }
    });
    return mongoose.model('Lote', schema, 'lotes');
};
