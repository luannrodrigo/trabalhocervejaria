const mongoose = require('mongoose');


module.exports = function(){
    const schema = mongoose.Schema({
        fermento:{
            type: String,
            required: true
        },
        lupulo:{
            type: String,
            required: true
        },
        malte:{
            type: String,
            required: true
        },
        agua:{
            type: String,
            required: true
        }
    });
    return mongoose.model('LoteMateriaPrima', schema, 'loteMateriaPrimas');
};
