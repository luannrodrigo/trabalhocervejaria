const mongoose = require('mongoose');

module.exports = function () {

    const schema = mongoose.Schema({
        receita: {
            type: mongoose.SchemaTypes.ObjectId,
            ref:'Receita',
            required: true
        },
        lote:{
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Lote',
            required: true
        }
    });
    return mongoose.model('Produto', schema, 'produtos');
}
