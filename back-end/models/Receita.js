const mongoose = require('mongoose');

module.exports = function(){
    const schema = mongoose.Schema({
        fermento:{
            type: Number,
            required: true
        },
        lupulo:{
            type: Number,
            required: true
        },
        malte:{
            type: Number,
            required: true
        },
        agua:{
            type: Number,
            required: true
        },
        fornecedores:{
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Fornecedores',
            required: true
        },
        cervejeiro:{
            type: mongoose.SchemaTypes.ObjectId,
            required: true
        }
    });
    return mongoose.model('Receita', schema, 'receitas');
};
